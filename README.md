[whatsapp-clone-101.herokuapp.com](whatsapp-clone-101.herokuapp.com)

Featues: 


    1. User Authentication.
    2. Can add other users.
    3. Can create group and add other users.
    4. Message can be seen live.
    5. Last seen of other user.

Tech stack used: MERN and TailwindCSS

Language: Typescript.

Schema Design:

    User : {
            name:
            email:
            rooms: refer to room collection
            password: 
            lastseen: 
        }
    Room:  {
            roomname:
            members:  -> reference to the members
            isgroup:  -> true/false
            messages:  -> reference to the array of message 
            timeStamp -> for room creation time
        }
    Message: {
            text:  
            sentBy : reference to user collection (id ,name)
            room: reference to room collection
            timeStamp: to register the time when message got sent
        }

Frontend tools and library used:

    Redux-toolkit ,TailwindCSS, Redux-saga


