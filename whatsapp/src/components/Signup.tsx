import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { sagaActions } from "../sagas/sagaActions";
import { State } from "../redux/types";
import { useNavigate } from "react-router-dom";
import {Comp} from '../pages/Home'
const Signup = () => {
  const [name, setName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [confirmPassword, setConfirmPassword] = React.useState("");
  const loggedIn = useSelector((state: State) => state.user.isloggedIn);
  const navigate = useNavigate();
  if (loggedIn) {
    navigate("/home");
  }
  function validateEmail(email: string) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  const [err, setErr] = React.useState({
    name: false,
    email: false,
    password: false,
    confirmPassword: false,
  });
  const dispatch = useDispatch();
  return (
    <div className="w-[50%] h-screen flex flex-col m-auto items-center justify-center scale-150">
       <Comp/>
      <p className="text-2xl">signup page</p>
      <p>name</p>
      <input
        type="text"
        value={name}
        name="name"
        onChange={(e) => {
          setErr({ ...err, name: name.length > 0 ? false : true });
          setName(e.target.value);
        }}
        className="border-2 border-gray-500"
      />
      {err.name ? <p className="text-red-500">name is required</p> : null}
      <p>email</p>
      <input
        type="text"
        value={email}
        name="email"
        onChange={(e) => {
          setErr({
            ...err,
            email: validateEmail(e.target.value) ? false : true,
          });
          setEmail(e.target.value);
        }}
        className="border-2 border-gray-500"
      />
      {err.email ? <p className="text-red-500">email is not valid</p> : null}
      <p>password</p>
      <input
        type="text"
        value={password}
        name="password"
        onChange={(e) => {
          setErr({
            ...err,
            password: e.target.value.length > 6 ? false : true,
          });
          setPassword(e.target.value);
        }}
        className="border-2 border-gray-500"
      />
      {err.password ? (
        <p className="text-red-500">password should be minimum 6 chars</p>
      ) : null}
      <p>confirm password</p>
      <input
        type="text"
        value={confirmPassword}
        name="confirm password"
        onChange={(e) => {
          setErr({ ...err, confirmPassword: e.target.value !== password });
          setConfirmPassword(e.target.value);
        }}
        className="border-2 border-gray-500"
      />
      {err.confirmPassword ? (
        <p className="text-red-500">Passwords do not match</p>
      ) : null}
      <button
        onClick={() => {
          if (
            name.length > 0 &&
            validateEmail(email) &&
            password.length > 6 &&
            password === confirmPassword
          ) {
            dispatch({
              type: sagaActions.SIGNUP,
              payload: { name, email, password },
            });
          } else {
            setErr({
              ...err,
              name: name.length === 0,
              email: !validateEmail(email) ,
              password: password.length > 6 ? false : true,
              confirmPassword: password === confirmPassword ? false : true,
            });
          }
        }}
        className="border-2 border-rose-500"
      >
        submit
      </button>
      <button>
        <Link to="/login">Login</Link>
      </button>
    </div>
  );
};

export default Signup;
