import React from "react";
import { Avatar } from "@material-ui/core";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import InsertEmoticIcon from "@material-ui/icons/InsertEmoticon";
import MicIcon from "@material-ui/icons/Mic";
import { useState } from "react";
import { useContext } from "react";
import { AppContext } from "../index";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { useEffect, useCallback, useMemo } from "react";
import { userActions } from "../redux/store";
import { createSelector } from "@reduxjs/toolkit";
import { useRef } from "react";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { Room } from "../redux/types";
import AddIcon from "@mui/icons-material/Add";
import { sagaActions } from "../sagas/sagaActions";
import { StringifyOptions } from "querystring";
const AlwaysScrollToBottom = () => {
  const elementRef: any = useRef<HTMLDivElement>(null);
  useEffect(() => {
    elementRef?.current.scrollIntoView();
  });
  return <div ref={elementRef}></div>;
};
const Conversation = ({ userId, chat }: { userId: string; chat: Room }) => {
  const time = (time:string) => {
    let diff = new Date().valueOf()  - Date.parse(time);
    console.log(diff, "difference");
    let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    let hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((diff % (1000 * 60)) / 1000);

    if (minutes == 0) {
      return seconds + " seconds ago";
    }
    if (hours == 0) {
      return minutes + " m " + seconds + " s ago";
    }
    if (days == 0) {
      return hours + " h ago";
    }
    return days + " d ago";
  }
  const Component = () => {
    return (
      <div className="h-[75%] bg-yellow-200 relative overflow-y-scroll flex  flex-col">
        {chat?.messages.map((message: any, index: number) => {
          console.log(message.sentBy._id, userId);
          let sentByme = message.sentBy._id === userId;
          return (
            <div
              className={"w-[50%] min-h-[20%]  border-4  rounded-3xl "}
              key={index}
            >
              <p>{message.text}</p>
              <p>
                {time(message.timeStamp)}{" "}
                {/* {new Date(message.timeStamp).getDate()}/
                {new Date(message.timeStamp).getMonth() + 1} :{" "} */}
                by {sentByme ? "me" : message.sentBy.name}
              </p>
            </div>
          );
        })}
        <AlwaysScrollToBottom />
      </div>
    );
  };
  return <Component />;
};
export const MemoizedConversation = React.memo(Conversation);
const Chat = ({ index }: { index: any }) => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [email, setEmail] = useState("");
  const [showArrow, setShowArrow] = useState(false);
  const [status, setStatus] = useState("please connect to internet");
  function validateEmail(email: string) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
  const addMember = () => {
    if (validateEmail(email)) {
      dispatch({
        type: sagaActions.ADD_MEMBER,
        payload: { roomId: chat._id, email },
      });
    } else {
      alert("please enter valid email");
    }
    setShowModal(false);
  };
  const [roomsLoaded, setRoomsLoaded] = useState(false);
  const [message, setMessage] = useState("");
  const chat = useSelector(
    (state: any) => state.user.rooms[index],
    shallowEqual
  ); // checks for equality in data as everytime action dispatched it will make a rerender
  const userId = useSelector((state: any) => state.user._id);
  const [showMembers,setShowmembers]=useState(false);
  const socket = useContext(AppContext);
  const handleKeydown = (event: any) => {
    if (event.key === "Enter") {
      if (socket.connected)
        socket.emit("send-message", message, chat._id, userId);
      setMessage("");
    }
  };

  useEffect(() => {
    if (socket.connected && !chat?.isgroup) {
      var interval = setInterval(() => {
        socket.emit("checkOnline", chat._id);
      }, 1000);
      socket.on("online", (data: any) => {
        console.log(data);
        setStatus("Online");
      });
      socket.on("offline", (data: any) => {
        setStatus("Offline");
      });
    }
    return () => {
      console.log("clearing interval");
      socket.removeAllListeners("online");
      socket.removeAllListeners("offline");
      clearInterval(interval);
    };
  }, [chat?._id]);

  if (chat?._id === undefined) {
    return <p>create a new room to start Conversation</p>;
  }
  return (
    <div className="w-[70vw] h-full relative">
      <div
        className={
          showModal ? "absolute top-0 m-auto w-screen h-screen z-10" : "hidden"
        }
      >
        <div className="h-[40%] w-[50%] m-auto border-4 z-10 flex justify-around items-center flex-col border-gray-500 bg-gray-50">
          <p>add member via email</p>
          <input
            className="h-10 p-2 rounded-3xl"
            type="text"
            placeholder="enter the user email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <button onClick={addMember}>add</button>
          <button onClick={() => setShowModal(false)}>close</button>
        </div>
      </div>
      <div className="h-[15%] flex items-center justify-around">
        <div className="w-[40%]  flex ">
          <Avatar />
          <div className="pl-[10px]">
            <p className="text-2xl">
              {!chat?.isgroup
                ? chat?.members.filter(
                    (member: any) => member._id !== userId
                  )[0]?.name
                : chat?.roomname}
            </p>
            {/* {!chat?.isgroup && status !== "online" && (
              <p className="text-gray-500">
                last seen at...
                {
                  chat?.members.filter(
                    (member: any) => member._id !== userId
                  )[0]?.lastseen
                }{" "}
              </p>
            )} */}
            {!chat?.isgroup && <p>{status}</p>}
            {chat?.isgroup && <p onClick={()=>setShowmembers(true)}>{chat.members.length} members</p>}
          </div>
        </div>
        <div className="w-[30%] scale-150 flex justify-around ">
          {/* <SearchOutlinedIcon /> */}
          {/* <AttachFileIcon /> */}
          {/* <MoreVertIcon /> */}
          {chat.isgroup && (
            <AddIcon
              className="cursor-pointer"
              onClick={() => setShowModal(true)}
            />
          )}
        </div>
      </div>
      <MemoizedConversation userId={userId} chat={chat} />
      <div className="flex h-[10%] items-center justify-around ">
        <InsertEmoticIcon className="scale-150" />
        <input
          type="text"
          placeholder="Type a message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          onKeyDown={handleKeydown}
          className="p-2 rounded-3xl w-[80%]"
        />
        {message.length > 0 ? (
          <ArrowForwardIcon className="scale-150" />
        ) : (
          <MicIcon className="scale-150" />
        )}
      </div>
      {showMembers && (<div className="h-screen w-screen bg-yellow-500 z-20">
          <p>show members</p>
          <p onClick={()=>setShowmembers(false)}>close</p>
      </div>)
      }
    </div>
  );
};

export default Chat;
