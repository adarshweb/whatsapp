import React from 'react'
import { Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux';

const Sidebarchat = ({data,index, changeIndex}:{data:any,index:number,changeIndex:any}) => {
    const userId = useSelector((state: any) => state.user._id);
    const temp=!data?.isgroup?data.members.filter((member:any)=>member._id!==userId)[0]?.name:data.roomname;
    console.log(data)
    return (
        <div onClick={()=>changeIndex(index)} className="h-[150px] border-4 flex justify-around items-center">
            <Avatar className='scale--'/>
            <div className='w-[70%] '>
                <p className='font-bold p-1'>
                    {temp}                                              
                </p>
                {/* <p className='p-1'>
                    Last seen
                </p> */}
            </div>
        </div>
    )
}

export default Sidebarchat
