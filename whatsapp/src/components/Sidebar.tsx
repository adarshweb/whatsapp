import React from "react";
import { Avatar } from "@material-ui/core";
import DonutLargeIcon from "@mui/icons-material/DonutLarge";
import ChatIcon from "@mui/icons-material/Chat";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { useEffect } from "react";
import SearchIcon from "@mui/icons-material/Search";
import { useContext } from "react";
import Sidebarchat from "./Sidebarchat";
import AddIcon from "@mui/icons-material/Add";
import { useDispatch } from "react-redux";
import { sagaActions } from "../sagas/sagaActions";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import { useSelector } from "react-redux";
import LogoutIcon from "@mui/icons-material/Logout";
import { State } from "../redux/types";
import GroupAdd from "@mui/icons-material/GroupAdd";
import { toastsActions } from "../redux/store";
import { AppContext } from "../index";
import ContentLoader, { Facebook } from "react-content-loader";
import { userActions } from "../redux/store";
import { useRef } from "react";
const Sidebar = ({ index, changeIndex }: { index: any; changeIndex: any }) => {
  const [showModal, setShowModal] = React.useState(false);
  const [groupModal, setGroupModal] = React.useState(false);
  const rooms = useSelector((state: State) => state.user.rooms);
  const [groupName, setGroupname] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [emailErr, setEmailerr] = React.useState(false);

  const loader = useSelector((state: State) => state.user.loadingRooms);
  const dispatch = useDispatch();
  const socket = useContext(AppContext);
  function validateEmail(email: string) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  // useEffect(() => {
  //   window.addEventListener("click", () => {
  //     if (showModal) {
  //       setShowModal(false);
  //     }
  //     if (groupModal) {
  //       setGroupModal(false);
  //     }
  //   });
  // });
  const createRoom = () => {
    if (validateEmail(email)) {
      setShowModal(false);
      dispatch({ type: sagaActions.CREATE_ROOM, payload: email });
    } else {
      setShowModal(false);
      // dispatch(toastsActions.addToast({msg:"invalid email",type:"error"}))
      alert("please enter valid email");
    }
  };
  const createGroup = () => {
    if (groupName.length > 0) {
      dispatch({
        type: sagaActions.CREATE_GROUP,
        payload: { room: groupName },
      });
      setGroupModal(false);
    } else {
      // dispatch(toastsActions.addToast({msg:"invalid email",type:"error"}))
      alert("please enter the group name");
    }
  };
 
  useEffect(() => {
    console.log("inside of side bar use effect");

    if (socket.connected) {
      rooms.map((room) => {
        console.log(room._id, "room id");
        socket.emit("join", room._id);
      });
      socket.on("joined", (data: any, room) => {
        console.log(data, room, "joined room successfully");
      });
      socket.on("message", (data: any, id: string) => {
        console.log(data, "message received");
        dispatch(userActions.addMessage({ id: id, message: data }));
      });
    }
    return () => {
      console.log(socket);
      socket.removeAllListeners();
    };
  }, [rooms.length !== 0]);
  return (
    <div className="w-[30vw] border-4 border-black-500 h-full relative">
      <div className="flex h-[10%] border-4 justify-around items-center">
        <Avatar />
        <div className="flex  scale-120 justify-between  w-[50%]">
          {/* <DonutLargeIcon/>  */}
          <GroupAddIcon
            className="cursor-pointer"
            onClick={() => setGroupModal(true)}
          />
          {/* <ChatIcon /> */}
          {/* <MoreVertIcon className="cursor-pointer"/> */}
          <LogoutIcon
            className="cursor-pointer"
            onClick={() => {
              dispatch({ type: sagaActions.LOGOUT });
              socket.disconnect();
            }}
          />
          <AddIcon
            className="cursor-pointer"
            onClick={() => setShowModal(true)}
          />
        </div>
      </div>
      {loader && <ContentLoader width={100} height={100} />}
      {!loader && (
        <>
          <div className="h-[5%] flex justify-around items-center border-4">
            <SearchIcon />
            <input
              className="w-[80%]"
              type="text"
              placeholder="Search or start a new chat"
            />
          </div>
          <div className="h-[800px] overflow-scroll">
            {rooms.map((room: any, i: number) => {
              return (
                <div
                  className={index == i ? "bg-gray-300" : "null"}
                  key={room._id}
                  // ref={(el) => {
                  //   element.current[i] = el;
                  // }}
                >
                  <Sidebarchat
                    data={room}
                    index={i}
                    changeIndex={changeIndex}
                  />
                </div>
              );
            })}
          </div>
        </>
      )}
      <div
        className={
          showModal ? "absolute top-0  w-screen h-screen z-10" : "hidden"
        }
      >
        <div className="h-[40%] w-[50%] m-auto border-4 z-10 flex justify-around items-center flex-col border-gray-500 bg-gray-50">
          <p>add user via email</p>
          <input
            className="h-10 p-2 rounded-3xl"
            type="text"
            placeholder="enter the user email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <button onClick={createRoom}>add</button>
          <button onClick={() => setShowModal(false)}>close</button>
        </div>
      </div>
      <div
        className={
          groupModal ? "absolute top-0  w-screen h-screen z-10" : "hidden"
        }
      >
        <div className="h-[40%] w-[50%] m-auto border-4 z-10 flex justify-around items-center flex-col border-gray-500 bg-gray-50">
          <p>with registered users only</p>
          <input
            className="h-10 p-2 rounded-3xl"
            type="text"
            value={groupName}
            onChange={(e) => setGroupname(e.target.value)}
            placeholder="enter the name of group"
          />
          {/* <input className="h-10 p-2 rounded-3xl" type="text" placeholder="user email" value={email} onChange={(e)=>setEmail(e.target.value)}/> */}
          <button onClick={createGroup}>create Group</button>
          <button onClick={() => setGroupModal(false)}>close</button>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
