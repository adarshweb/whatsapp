import React from "react";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { sagaActions } from "../sagas/sagaActions";
import { State } from "../redux/types";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { Comp } from "../pages/Home";
import { ClassNames } from "@emotion/react";
const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const loggedIn = useSelector((state: State) => state.user.isloggedIn);

  const [password, setPassword] = useState("");
  function validateEmail(email: string) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  const [email, setEmail] = useState("");
  const [invalidEmail, setInvalidEmail] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  useEffect(() => {
    if (loggedIn) {
      navigate("/");
    }
    return () => {};
  }, [loggedIn]);
  return (
    <div className="w-[50%] h-screen flex flex-col m-auto items-center justify-center scale-150">
      <Comp />
      <p>email</p>
      <input
        type="text"
        name="email"
        value={email}
        onChange={(e) => {
          setEmail(e.target.value);
          setInvalidEmail(!validateEmail(e.target.value));
        }}
        className="border-2 border-gray-500"
      />
      {invalidEmail ? <p className="text-red-500">email not valid</p> : null}
      <p>password</p>
      <input
        type="text"
        name="password"
        value={password}
        onChange={(e) => {
          setPassword(e.target.value);
          setInvalidPassword(e.target.value.length < 6);
        }}
        className="border-2 border-gray-500"
      />
      {invalidPassword ? (
        <p className="text-red-500">password should be minimum 6 chars</p>
      ) : null}
      <button
        onClick={() => {
          if (validateEmail(email) && password.length > 0) {
            dispatch({ type: sagaActions.LOGIN, payload: { email, password } });
          } else {
            setInvalidEmail(!validateEmail(email));
            setInvalidPassword(password.length < 6);
          }
        }}
      >
        submit
      </button>
      <button>
        <Link to="/signup">signup</Link>
      </button>
    </div>
  );
};

export default Login;
