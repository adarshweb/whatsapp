import React from 'react'
import Sidebar from '../components/Sidebar'
import Chat from '../components/Chat'
import { useEffect } from 'react'
import {useDispatch} from 'react-redux'
import { sagaActions } from '../sagas/sagaActions';
import { State } from '../redux/types';
import { io } from 'socket.io-client'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import { toastsActions } from '../redux/store'
import { useContext } from 'react';
import { AppContext } from '../index';

export const Comp=()=>{
    const dispatch=useDispatch();
    const messageOnalert=useSelector((state:any)=>{
        return state.toasts;
    })
    useEffect(()=>{
       if(messageOnalert.msg!==""){
           Swal.fire(messageOnalert.msg)
           dispatch(toastsActions.clearToast())
       }
    },[messageOnalert])
return (<></>)
}

const Home = () => {
    const navigate=useNavigate();
    const dispatch=useDispatch()
    const loggedIn=useSelector((state:State)=>state.user.isloggedIn);
    const userId=useSelector((state:State)=>state.user._id);
    const [index,setIndex]=React.useState(0)
    const changeIndex=(index:number)=>{
        setIndex(index)
    }
    const socket=useContext(AppContext);
    useEffect(() => {
        if(!loggedIn){
            navigate('/')
        }
        else{
            socket.emit("save-user-id", userId)
        }
        return () => {
        }
    }, [loggedIn])
    return (
        <div className='h-screen bg-gray-100 w-[90vw] m-auto flex place-content-around border-3'>
            <Comp/>
            <Sidebar index={index} changeIndex={changeIndex}/>
            <Chat index={index}/>
        </div>
    )
}
// namespace user_id . room name is id of other user or group name
export default Home
