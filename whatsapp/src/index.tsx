import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Login from './components/Login';
import reportWebVitals from './reportWebVitals';
import Chat from './components/Chat';
import {BrowserRouter as Router ,Routes, Route, Link, useParams} from 'react-router-dom';
import Signup from './components/Signup';
import {Provider} from 'react-redux';
import store from './redux/store';
import { createContext } from 'react';
import Home from './pages/Home';
import { io } from 'socket.io-client';
import { sagaActions } from './sagas/sagaActions';

const socket=io(window.location.origin)
// socket.on('connect',()=>{
//     console.log('connected')
// })

export const AppContext = createContext(socket);
store.dispatch({type:sagaActions.GET_STATE});
ReactDOM.render(
  <Provider store={store}>
  <Router>
    <Routes>
      <Route path="/" element={<App />}/>
      <Route path="/login" element={<Login/>}/>
      <Route path="/signup" element={<Signup/>}/>
      <Route path="/home" element={
        <AppContext.Provider value={socket}>
          <Home/>
        </AppContext.Provider>
      }/>
      <Route path="*" element={<div>Not found</div>}/>
    </Routes>
  
  </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
