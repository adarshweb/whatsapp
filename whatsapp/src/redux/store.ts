import {
  configureStore,
  createSlice,
} from "@reduxjs/toolkit";
import { initialState } from "./initialState";
import createSagaMiddleware from "redux-saga";
import userSaga from "../sagas/userSaga";
const userSlice: any = createSlice({
  name: "user",
  initialState: initialState.user,
  reducers: {
    setUser: (state, action) => {
      console.log(action,"action")
      return {...state, ...action.payload, isloggedIn:true, loadingLogin:false, loginErrorMessage:''};
    },
    loadingLogin: (state, action) => {
      state.loadingLogin = action.payload;
    },
    loadingRooms: (state, action) => {
    state.loadingRooms = action.payload;
    },
    setLoadingRooms: (state, action) => {
      state.loadingRooms = action.payload.loadingRooms;
    },
    loginError: (state, action) => {
      state.loginErrorMessage = action.payload.loginErrorMessage;
      state.loadingLogin=false;
    },
    setRoomsData: (state, action) => {
      state.rooms = action.payload;
    },
    addMessage: (state, action) => {
      console.log(action.payload,"action")
      // state.rooms[action.payload.index].messages.push(action.payload.message);
      state.rooms.map(room=>{
        if(room._id===action.payload.id){
          room.messages.push(action.payload.message);
        }
      })
    },
    addNewRoom: (state, action) => {
      state.rooms.push(action.payload);
    },
    addnewGroup: (state, action) => {
      state.rooms.push(action.payload);
    },
    logout: (state, action) => {
      state.isloggedIn = false;
      state.rooms = [];
      state._id="";
      state.email="";
    },
    addMember: (state, action) => {
      state.rooms.map(room=>{
        console.log(room)
        if(action.payload._id===room?._id){
          room.members.push(action.payload.member);
        }
      })
    }
  },
});
const toasts:any=createSlice({
  name:'toasts',
  initialState:{msg:'',type:'success',show:false},
  reducers:{
    setToast: (state,action)=>{
      return {...state, ...action.payload};
    },
    clearToast:(state,action)=>{
      state.msg="";
      state.show=false;
    }
  }
})
export const userActions = userSlice.actions;
export const toastsActions=toasts.actions;
let sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: {
    user: userSlice.reducer,
    toasts:toasts.reducer
  },
  middleware: (getDefaultMiddleware) => {
    return [sagaMiddleware, ...getDefaultMiddleware({ thunk: false })];
  },
});
sagaMiddleware.run(userSaga);

export default store;
