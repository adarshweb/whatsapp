export interface State{
    user: User
}
interface User{
    name:string,
    email:string,
    _id:string,
    isloggedIn:boolean
    rooms: Room[]
    loadingRooms: boolean,
    loginErrorMessage: string,
    loadingLogin: boolean,
    searchtext:string
}
interface Message{
    id:string,
    sentBy:string,
    timeStamp:string,
    seen:boolean,
    media:string,
    text:string
}
interface member{
    _id:string,
    name:string,
    email:string
}
export interface Room{
    _id:string,
    roomname:string
    messages:Message[]
    isgroup:boolean
    members:member[]
    timeStamp:string
    createdBy:string
}