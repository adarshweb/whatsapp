import {State} from './types';
export const initialState:State = {
    user:{
        name:"",
        _id:"",
        email:"",
        isloggedIn:false,
        loginErrorMessage:"",
        loadingLogin:false,
        rooms:[],
        loadingRooms:false,
        searchtext:""
    },
}