import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { useEffect } from "react";
import { State } from "./redux/types";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { sagaActions } from "./sagas/sagaActions";
import { Socket } from "dgram";
import { AppContext } from "./index";
import { useContext } from "react";
function App() {
  const loggedIn = useSelector((state: State) => state.user.isloggedIn);
  const dispatch = useDispatch();
  const userid = useSelector((state: State) => state.user._id);
  const socket = useContext(AppContext);
  useEffect(() => {
    if (loggedIn) {
      socket.connect();
      socket.on("connect", () => {
        console.log("connected");
      });
      dispatch({ type: sagaActions.GET_ROOMS_DATA });
    } else {
      socket.disconnect();
      console.log(socket)
    }
    return () => {
      socket.removeAllListeners();
    }
  }, [loggedIn]);
  return (
    <div className="flex justify-around h-[50%] mt-10 w-[50%] m-auto border-4 font-bold text-2xl">
      {!loggedIn && (
        <>
          <Link to="/login">
            <p className="hover:bg-blue-500 hover:text-white">login</p>
          </Link>
          <Link to="/signup">
            <p className="hover:bg-blue-500 hover:text-white">signup</p>
          </Link>
        </>
      )}
      {loggedIn && (
        <Link to="/home">
          <p className="hover:bg-blue-500 hover:text-white">Go to app</p>
        </Link>
      )}
    </div>
  );
}

export default App;
