export const sagaActions = {
  LOGIN: "LOGIN",
  GET_ROOMS_DATA: "GET_ROOM_DATA",
  SIGNUP: "SIGNUP",
  CREATE_ROOM: "CREATE_ROOM",
  GET_STATE: "GET_STATE",
  CREATE_GROUP: "CREATE_GROUP",
  LOGOUT: "LOGOUT",
  ADD_MEMBER: "ADD_MEMBER",
};
