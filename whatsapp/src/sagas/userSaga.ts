import { takeEvery, put, call, all, takeLatest } from "redux-saga/effects";
import { sagaActions } from "./sagaActions";
import { api } from "../api";
import { toastsActions, userActions } from "../redux/store";

export interface ResponseGenerator {
  config?: any;
  data?: any;
  headers?: any;
  request?: any;
  status?: number;
  statusText?: string;
}
function* login(action: any) {
  try {
    yield put(userActions.loadingLogin(true));
    const response: ResponseGenerator = yield api.login(action.payload);
    if (response.status === 401) {
      yield put(userActions.setLoginErrorMessage(response.data.message));
      yield put(
        toastsActions.setToast({
          msg: "Error in Logging in",
        })
      );
    } else if (response.status === 200) {
      console.log(response.data, response.status);
      yield put(userActions.setUser(response.data.user));
      yield put(userActions.loadingLogin(false));
      // yield put(toastsActions.setToast({
      //   msg: "Logged in Successfully",
      // }));
    }
  } catch (error) {
    console.log(error);
    yield put(userActions.loginError(error));
    yield put(
      toastsActions.setToast({
        msg: "Error in Logging in",
      })
    );
  }
}
function* roomsData() {
  try {
    yield put(userActions.loadingRooms(true));
    const response: ResponseGenerator = yield api.getRoomsData();
    yield put(userActions.setRoomsData(response.data.rooms));
    yield put(userActions.loadingRooms(false));
  } catch (err) {
    console.log(err);
  }
}
function* signup(action: any) {
  try {
    const response: ResponseGenerator = yield api.signup(action.payload);
    if (response.status === 200) {
      yield put(
        toastsActions.setToast({
          msg: "Signed up Successfully",
          type: "success",
          show: true,
        })
      );
    }
  } catch (err) {
    console.log(err);
    yield put(
      toastsActions.setToast({
        msg: "Error in Signing up",
      })
    );
  }
}
function* createRoom(action: any) {
  try {
    const response: ResponseGenerator = yield api.createRoom({
      email: action.payload,
    });
    if (response.status === 200) {
      yield put(userActions.addNewRoom(response.data.room));
      yield put(
        toastsActions.setToast({
          msg: "Room Created Successfully",
          type: "success",
          show: true,
        })
      );
    }
  } catch (err: any) {
    yield put(
      toastsActions.setToast({
        msg: "Error in creating room",
        type: "error",
        show: true,
      })
    );
  }
}
function* getState(action: any) {
  try {
    const response: ResponseGenerator = yield api.getState();
    if (response.status === 200) {
      yield put(userActions.setUser(response.data.user));
    }
  } catch (err) {
    console.log(err, "uihgiuh");
  }
}
function* createGroup(action: any) {
  try {
    const reponse: ResponseGenerator = yield api.createGroup(action.payload);
    if (reponse.status === 200) {
      yield put(userActions.addnewGroup(reponse.data.room));
      yield put(
        toastsActions.setToast({
          msg: "Group Created Successfully",
          type: "success",
          show: true,
        })
      );
    }
  } catch (err) {
    console.log(err);
  }
}
function* logout(action: any) {
  try {
    const response: ResponseGenerator = yield api.logout();
    if (response.status === 200) {
      yield put(userActions.logout());
    }
  } catch (err) {
    console.log(err);
  }
}
function* addMember(action: any) {
  try {
    const response: ResponseGenerator = yield api.addMember(action.payload);
    console.log(response);
    if (response.status === 200) {
      yield put(
        userActions.addMember({
          member: response.data,
          _id: action.payload.roomId,
        })
      );
    }
  } catch (err) {
    console.log(err);
  }
}
export default function* userSaga() {
  yield all([
    takeLatest(sagaActions.LOGIN, login),
    takeLatest(sagaActions.GET_ROOMS_DATA, roomsData),
    takeLatest(sagaActions.SIGNUP, signup),
    takeLatest(sagaActions.CREATE_ROOM, createRoom),
    takeLatest(sagaActions.GET_STATE, getState),
    takeLatest(sagaActions.CREATE_GROUP, createGroup),
    takeLatest(sagaActions.LOGOUT, logout),
    takeLatest(sagaActions.ADD_MEMBER, addMember),
  ]);
}
