import axios from 'axios';

export const api={
    login: (data:{})=>{
        return axios.post('api/login',data,{withCredentials: true});
    },
    signup: (data:{})=>{
        return axios.post('api/signup',data);
    },
    createRoom:(data:{})=>{
        return axios.post('api/create-room',data,{withCredentials:true});
    },
    getRoomsData:()=>{
        return axios.get('api/get-rooms-data',{withCredentials:true});
    },
    getState:()=>{
        return axios.get('api/get-state',{withCredentials:true});
    },
    createGroup:(data:{})=>{
        return axios.post('api/create-group',data,{withCredentials:true});
    },
    logout:()=>{
        return axios.get('api/logout',{withCredentials:true});
    },
    addMember:(data:{})=>{
        console.log("inside frontend add member",data)
        return axios.post('api/add-member',data,{withCredentials:true});
    }
}