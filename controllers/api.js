const User = require("../models/user");
const Room = require("../models/room");
const verifyCookie = require("../middlewares/verify");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const room = require("../models/room");
const { response } = require("express");
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log(req.body);
    const user = await User.findOne({ email });
    console.log(user);
    if (!user) {
      res.status(400).send("Invalid email");
    } else {
      if (bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET, {
          expiresIn: "5h",
        });
        res.cookie("token", token, {
          httpOnly: true,
          maxAge: 1000 * 60 * 60 * 5,
        });
        res.status(200).send({
          msg: "Login Successful",
          user: { _id: user._id, name: user.name, email: user.email, token },
        });
      } else {
        res.status(401).send("Invalid password");
      }
    }
  } catch (e) {
    console.log(e);
    res.status(401).send("error");
  }
};
exports.signup = async (req, res) => {
  try {
    console.log(req.body);
    email = req.body.email.toLowerCase();
    password = req.body.password;
    const user = await User.findOne({ email });
    console.log(user);
    if (!user) {
      let hashedPassword = bcrypt.hashSync(password, 10);
      const newUser = new User({
        name: req.body.name,
        email,
        password: hashedPassword,
      });
      await newUser.save();
      res.status(200).send({msg:"Signup Successful"});
    } else {
      res.status(400).send({msg:"User already exists"});
    }
  } catch (err) {
    console.log(err);
    res.status(401).send({msg:"error"});
  }
};
exports.getRoomsData = async (req, res) => {
  try {
    //console.log(req._id);
    const user = await User.findById(req._id);

    await user.populate({ path: "rooms" });
    if(user.isGroup){
      await user.populate({ path: "rooms.members", select: "name email" });
    }
    else{
    await user.populate({ path: "rooms.members", select: "name email lastseen" });
    }
    await user.populate({ path: "rooms.createdBy", select: "name email" });
    await user.populate("rooms.messages");
    await user.populate({ path: "rooms.messages.sentBy", select: "name" });
    res.status(200).send({ rooms: user.rooms });
  } catch (err) {
    console.log(err);
    res.status(401).send("error");
  }
};
exports.createRoom = async (req, res) => {
  try {
    const status = 200,
      msg = "";
    // if user does not exist in database return error
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      res.status(400).send({ msg: "user does not exist" });
    } else {
      // if user exists and room already created
      console.log(user);
      let flag = 0;
      await user.populate({ path: "rooms", match: { isGroup: false } });
      // if group created do not check for room already created
      user.rooms.forEach((room) => {
        room.members.map((member) => {
          console.log("inside for each");
          console.log(member.toString(), req._id.toString());
          if (member.toString() === req._id.toString()) {
            console.log(member.toString(), req._id);
            flag = 1;
          }
        });
      });
      if (flag === 1) {
        res.status(400).send({ msg: "room already created" });
        return;
      }
      console.log("after for each");
      // if user exists and room does not exist
      const newRoom = new Room({
        members: [req._id, user._id],
        createdBy: req._id,
      });
      await newRoom.save();
      user.rooms.push(newRoom); // push new room to user's rooms array
      await user.save();
      const me = await User.findById(req._id);
      me.rooms.push(newRoom); // push new room to my rooms array
      await me.save();
      const temp=await newRoom.populate({ path: "members", select: "name email lastseen" });
      res.status(200).send({ msg: "Room created successfully", room: temp });
    }
  } catch (err) {
    console.log(err);
    res.status(401).send({ msg: "error" });
  }
};
exports.getState = async (req, res) => {
  try {
    const user = await User.findById(req._id);
    res.status(200).send({
      msg: "user logged in",
      user: { _id: user._id, name: user.name, email: user.email },
    });
  } catch (err) {
    console.log(err);
    res.status(401).send("error");
  }
};

exports.createGroup = async (req, res) => {
  try {
    const room = new Room({
      isgroup: true,
      roomname: req.body.room,
      members: [req._id],
      createdBy: req._id,
    });
    await room.save();
    const user = await User.findById(req._id);
    user.rooms.push(room);
    await user.save();
    await room.populate({ path: "members", select: "name email" });
    const temp=await room.populate({ path: "createdBy", select: "name email" });
    res.status(200).send({ msg: "group created successfully", room: temp });
  } catch (err) {
    console.log(err);
  }
};
exports.logout = async (req, res) => {
  res.clearCookie("token");
  res.status(200).send({ msg: "logged out" });
};
exports.addUserToGroup=async (req,res)=>{
  try{
    const room =await Room.findById(req.body.roomId);
    const user =await User.findOne({email:req.body.email});
    if(room.members.filter(member=>member.toString===user._id).length>0){
      res.status(400).send({msg:"user already exists in group"});
    }
    else{
      room.members.push(user._id);
      await room.save();
      user.rooms.push(room);
      await user.save();
      res.status(200).send({_id:user._id,name:user.name,email:user.email});
    }
  }catch(err){
    res.status(400).send({msg:"error"});
  }
}