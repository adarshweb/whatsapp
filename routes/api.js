
const router=require('express').Router();
const userController =require('../controllers/api');
const verifyCookie = require('../middlewares/verify');
router.post('/login',userController.login)
router.post('/signup',userController.signup)
router.get('/get-rooms-data',verifyCookie,userController.getRoomsData)
router.post('/create-room',verifyCookie,userController.createRoom)
router.get('/get-state',verifyCookie,userController.getState)
router.post('/create-group',verifyCookie,userController.createGroup)
router.get('/logout',userController.logout);
router.post('/add-member',verifyCookie,userController.addUserToGroup)
module.exports=router;