const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const roomSchema = new Schema({
  roomname: {
    type: String,
  },
  members: {
    type: [Schema.Types.ObjectId],
    ref: "User", // populate with user name email userid FINAL
  },
  isgroup: {
    type: Boolean,
    default: false,
  },
  messages: {
    type: [Schema.Types.ObjectId], // name of user who sent message which will be shown to user
    ref: "Message",
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});
module.exports = mongoose.model("Room", roomSchema);
