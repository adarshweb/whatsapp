const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const messageSchema=new Schema({
    text:{
        type:String,
    },
    sentBy:{
        type:Schema.Types.ObjectId,
        ref:'User',
    },
    room:{
        type:Schema.Types.ObjectId,
        ref:'Room',
    },
    timeStamp:{
        type:Date,
        default:Date.now
    },
    seen:{
        type:Boolean,
        default:false
    },
    media:{
        type:String,
        default:null
    }
});
module.exports=mongoose.model('Message',messageSchema);