const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const userSchema=new Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    rooms:{
        type:[Schema.Types.ObjectId],
        ref:'Room',
        default:[]
    },
    password:{
        type:String,
        required:true
    },
    lastseen:{
        type:Date,
        default:new Date()
    }
},{strict:false})
module.exports=mongoose.model('User',userSchema);