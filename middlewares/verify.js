const User=require('../models/user');
const jwt=require('jsonwebtoken');
const verifyCookie=async (req,res,next)=>{
    try{
        const token=req.cookies.token;
        console.log("inside verify middleware")
        if(!token){
            res.status(401).send('unauthorised');
        }else{
            const decodedId=jwt.verify(token,process.env.TOKEN_SECRET)
            const user=await User.findById(decodedId._id);
            req._id=user._id;
            next();
        }
    }catch(error){
        console.log(error);
        res.status(401).send('error');
    }
}
module.exports=verifyCookie;