const express = require("express");
const Room = require("./models/room");
const mongoose = require("mongoose");
const http = require("http");
const path = require("path");
const { Server } = require("socket.io");
const api = require("./routes/api");
const cors = require("cors");
const Message = require("./models/message");
const cookieParser = require("cookie-parser");
var userids = {};
const User = require("./models/user");
mongoose.connect(
  "mongodb+srv://adarsh-admin:AoUJo2luTwjrCDHv@cluster0.jjs5s.mongodb.net/chat-app"
);
const PORT = process.env.PORT || 8080;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Mongodb Connection Error:"));
db.once("open", () => {
  console.log("Mongodb Connection Successful");
});
require("dotenv").config();
const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(cors({ credentials: true, origin: "http://localhost:3000" }));
const server = http.createServer(app);
const io = new Server(server, {
  cors: { credentials: true, origin: "http://localhost:3000" },
  pingTimeout: 30000,
});
app.use(express.static(__dirname + "/whatsapp/build"));

app.use("/api", api);
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname + "/whatsapp/build/index.html"));
});
io.on("connection", (socket) => {
  console.log("a user connected");
  socket.on("join", (room) => {
    socket.join(room);
    io.in(room).emit("joined", { msg: "joined", room });
    console.log(`${room} joined`);
  });
  socket.on("id", () => {
    console.log("id", socket.id);
  });
  socket.on("disconnect", (room) => {
    console.log("user disconnected");
    // io.emit('disconnected',{msg:'disconnected'});
  });
  socket.on("save-user-id", (data) => {
    //console.log("socket id saved",data)
    userids[socket.id] = data;
    console.log(userids);
  });
  socket.on("checkOnline", (room_id) => {
    
    console.log(io.sockets.adapter.rooms.get(room_id)?.size);
    if (io.sockets.adapter.rooms.get(room_id)?.size > 1) {
      console.log("online");
      socket.emit("online", { msg: "online" });
    } else {
      console.log("offline");
      socket.emit("offline", { msg: "offline" });
    }
  });
  socket.on("disconnect", async (reason) => {
    try {
      console.log(reason); // "ping timeout"
      if (userids[socket.id] !== null) {
        const user = await User.findById(userids[socket.id]);
        console.log(user);
        user.lastseen = new Date();
        await user.save();
        console.log("user disconnected");
      }
    } catch (er) {
      console.log(er);
    }
  });
  socket.on("send-message", async (message, chatId, userId) => {
    console.log(message);
    console.log(chatId);
    console.log(userId, "userId");
    let id = userId.trim();
    let sentById = mongoose.Types.ObjectId(id);
    console.log(sentById, "after sent by id creation");
    const newMessage = new Message({
      text: message,
      sentBy: sentById,
      room: chatId,
    });

    await newMessage.save();
    newMessage.populate({ path: "sentBy", select: "name" });
    console.log(newMessage);
    // now find the room and add the message to the room
    const room = await Room.findById(chatId);
    console.log(room);
    room.messages.push(newMessage);
    await room.save();
    io.in(chatId).emit("message", newMessage, chatId);
    // socket.to(chatId).emit('message',newMessage); this is to send to all the users in the room except the sender
  });
});
server.listen(PORT, () => {
  console.log("server is running on port 8000");
});
